<?php include ("inc/header.php");   ?>
  <body onload="onLoadChangeColor()">
    <!-- Preloader Start -->
    <div id="preloader">
      <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
      </div>
    </div>
    <!-- Preloader end -->

    <?php include ("inc/menu.php");   ?>
<!-- banner Start Here -->
<section class="banner overflow-hidden menuToShow" >
      <div class="banner-content" id="about-menu">
        <!-- <h3 class="text-uppercase">become a</h3> -->
        <!-- <h3 class="text-uppercase">fullstack developer</h3> -->
        <!-- <div
          class="fullstack py-2 py-lg-4"
          data-aos="fade-up-left"
          data-aos-duration="1000"
        > -->
          <!-- <h1 class="text-center text-uppercase"> -->
            <!-- Become a fullstack developer -->
            <!-- This is About Us Page
          </h1>
        </div>

        <div class="banner_text overflow-hidden">
          <p> -->
            <!-- Agile way to shape your software development career on Fullstack
            Software development track. Acquire knowledge and practical exposure
            about the latest software development trends, technologies and best
            practices. -->
            <!-- This is About Us Page
          </p>
          <div class="icon"> -->
            <!-- <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/icon.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661493251809"
              alt="icon-full-stack-developer"
              class="img-fluid"
            /> -->
          <!-- </div>

          <div class="btn_banner py-2">
           
          </div>
          <div class="banner_pc"> -->
            <!-- <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/banner-pc.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988773216"
              alt="banner-pc-full-stack-developer"
              class="img-fluid"
            /> -->
          </div>
        </div>
      </div>
    </section>

    <!-- banner Start End -->

      <!-- Work Section Start Here -->
      <section class="work overflow-hidden menuToShow" >
      <div class="hero">
        <h2 class="text-center" data-aos="zoom-in" data-aos-duration="1000">
          Not a <span>Training, </span><span>train-hire-deploy </span>Model
        </h2>
        <p class="w-50 text-center m-auto pb-3">
          Model is rapidly growing and substituting all conventional models,
          also known as talent-on-demand model. In this model training is
          completely different from orthodox software training.In our model
          candidates are going through scrupulous learn-develop-review mode
          learning with steady assessment.
        </p>
      </div>
      <div class="container">
        <div class="row">
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div>
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div>
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div>
          <!-- First Cercle -->
          <div
            class=" half-cirlce col-12 col-cercle col-md-6 col-lg-4 order-lg-first overflow-hidden p-4 "
            data-aos="fade-up-right"
            data-aos-duration="800"
          >
            <div class="cercle">
              <div class="yellow-cercle"></div>
              <div class="main-cercle">
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-1.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988683013"
                  alt="img-full-stack-developer"
                  class="img-fluid"
                />
                <h3>Train</h3>
                <p class="m-auto">
                  <!-- Training and development exposure on industry live projects -->
                  Training that doesn’t drown to meet today’s demands of the
                  industries.
                </p>
              </div>
            </div>
          </div>
          <!-- Second Cercle -->
          <div
            class=" half-cirlce col-12 col-cercle col-lg-4 order-md-first overflow-hidden p-4"
            data-aos="flip-left"
            data-aos-duration="800"
          >
            <div class="brown-cercle">
              <div class="cercle">
                <div class="yellow-cercle"></div>
                <div class="main-cercle">
                  <img
                    src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-2.png?ik-sdk-version=javascript-1.4.3&updatedat=1661490015242"
                    alt="img-full-stack-developer"
                    class="img-fluid"
                  />
                  <h3>Hire</h3>
                  <p class="m-auto">
                    <!-- Continuous assessment and evaluation during training to
                    hire. -->
                    Segments wearing veil of assessments to hire the best.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <!-- Third Circle -->
          <div
            class="half-cirlce col-12 col-cercle col-md-6 col-lg-4 overflow-hidden p-4"
            data-aos="fade-up-left"
            data-aos-duration="800"
          >
            <div class="cercle">
              <div class="yellow-cercle"></div>
              <div class="main-cercle">
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-3.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661489383024"
                  alt="img-full-stack-developer"
                  class="img-fluid"
                />
                <h3>Deploy</h3>
                <p class="m-auto">
                  <!-- Selected Champs will be deployed on customer or in-house
                  projects. -->
                  Tailored and basic training deploys the right candidate to the
                  right seat.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Work Section End Here-->


    <section class="product_item m-0 py-5 menuToShow" id="whyjschamps">
      <h2 class="text-uppercase d-none">our products</h2>
      <p class="text-center">
        Why <span>JSChamps</span> team is the <span> best choice</span> for
        <span>Fullstack Development</span>
      </p>
      <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
          <div class="col-md-12 p-0 m-0">
            <div class="row p-0 m-0">
              <div class="col-md-12 p-0 m-0 overflow-hidden">
                <div class="row p-0 m-0">
                  <div
                    class="col-md-6 first_col col-12 p-0 m-0"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_img m-0 p-0">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988732137"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                  <div
                    class="col-md-6 first_col_1 p-0"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_text p-0">
                      <p>
                        <!-- Learn from industry experts -->
                        JS Champs experts nurture the talent with the necessary
                        skills so that the candidates can become an asset and
                        not a liability to your business model.
                      </p>
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-12 main_col_third p-0 overflow-hidden">
                <div class="row product_row p-0 m-0">
                  <div
                    class="col-md-6 p-0 third_col"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_text_1">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                      <p>
                        <!-- Apply your learning and develop small software component -->
                        Our team of knowledgeable professionals is highly
                        trained in automation and other tools to educate for the
                        competition.
                      </p>
                    </div>
                  </div>
                  <div
                    class="col-md-6 col-12 p-0 third_col_1"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_img">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img_2.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988620517"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-12 p-0 main_fourth_col overflow-hidden">
                <div class="row p-0 m-0">
                  <div
                    class="col-md-6 first_col col-12 p-0"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_img">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img_3.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661489274528"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                  <div
                    class="col-md-6 p-0 first_col_1"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_text">
                      <p>
                        <!-- Get reviewed by experts and improvise your software
                        component -->
                        During the deployment resourcing, our team doubled check
                        the capability of every aptitude for 100% talent
                        retainability.
                      </p>
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Next Batch End -->
    <?php include ("inc/footer.php");   ?>
    

   