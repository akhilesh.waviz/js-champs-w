<?php
$baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
$siteurl = ($baseUrl === "http://localhost") ? "http://localhost/js-champs-w" : "https://jschamps.4up.in";
?>


<!DOCTYPE html>
<html  lang="en" xml:lang="en">
  <head>
    <title>Fullstack Development Training :JS Champs</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- CSS -->
    <link rel="stylesheet" type="text/css"  href="<?php echo $siteurl;?>/scss/style.css" />
    

    <!-- Bootstrap CSS v5.2.0-beta1 -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
      integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
      crossorigin="anonymous"
    />

    <!-- aos library -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Icons -->

    <!-- Favicon -->
    <link
      rel="icon"
      type="image/x-icon"
      href="https://ik.imagekit.io/wavizkushinagar/jschamps/fabicon.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498078839"
    />

    <!-- Meta Tag-->
    <meta name="title" content="Best Fullstack Development Training">
<meta name="description" content="JavaScript development training">
<meta name="keywords" content="Web development training, Fullstack development training on live projects, Front-end development, test driven development">
<meta name="robots" content="index, follow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="en">

<script src="https://www.google.com/recaptcha/api.js"></script>
<!-- google analitics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-9YJ1MW92QY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-9YJ1MW92QY');
</script>
<!-- search console -->
<meta name="google-site-verification" content="RvRx1qRA2NYP7I5rkVq72tU14m3B8wB20km5Ok7XgK0" />


  </head>