   <!-- Menu Start -->
   
    <nav id="nav" class="m-0 p-0 w-100 mt-sm-auto  nav">
      <div class="container_main p-0 m-0">
       <div class="container mb-0 pb-0">
       <div class="main_row p-0 m-0">
          <!-- Logo  -->
          
          <a class="logo_img" href="<?php echo $siteurl;?>"
            ><img
              src="<?php echo $siteurl;?>/image/js-logo.png"
              alt="logo-full-stack-developer"
              class="img-fluid"
          /></a> 
          <!-- Main menu -->
          <ul class="menu list-unstyled mb-0" id="menu">
            <li class="dyanmic-active-identifer">
              <a href="<?php echo $siteurl;?>" data-id="home" class="menu_active menua">Home</a>
            </li>
            <li class="dyanmic-active-identifer">
              <a href="<?php echo $siteurl;?>/about.php" data-id="about" >About</a>
            </li>
             <li>
              <a href="https://www.jschamps.com/blog/" data-id="blog" 
                >Blog</a
              >
            </li>
            <li class='service-l dyanmic-active-identifer'>
              <a href="" data-id="blog" 
                >Service</a
              >
              <div class='servic-menu'>
              <ul id='submenu'>
                <li><a href="#">Consulting</a>
                  <ul id='submenutwo'>
                    <li><a href="<?php echo $siteurl;?>/service/consulting/fullstack-web-development.php">Fullstack Web Development</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/consulting/design-system.php">Design System</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/consulting/front-end-development.php">Front-end Development</a></li>
                  </ul>
                </li>
                <li><a href="#">Training</a>
                  <ul id='submenutwo'>
                    <li><a href="<?php echo $siteurl;?>/service/training/fullstack-web-development-training-in-noida.php">Fullstack Web Development Training in Noida</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/react-js-training-in-noida.php">React JS Training in Noida</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/fullstack-web-development-training-in-lucknow.php">Fullstack Web Development Training in Lucknow</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/react-js-training-in-lucknow.php">React JS Training in Lucknow</a></li>
                  </ul>
                </li>
              </ul>
              </div>
            </li>
            <li class='f-d dyanmic-active-identifer'>
              <a href="">Fullstack Development <span></span>
              </a>
              <div id="fullstack-development">
              <ul class="ps-0">
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development/node-and-Express-with-react.php">Node and Express with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\java-and-spring-boot-with-react.php">Java and Spring Boot with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\python-and-fast-api-with-react.php">Python and Fast API with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\php-and-laravel-with-react.php">PHP and Laravel with React</a>
                </li>
              </ul>
  </div>
            </li>
                     

            <li class="dyanmic-active-identifer">
              <a href="<?php echo $siteurl;?>/contact.php" data-id="blog" 
                >
                Contact
                </a
              >
            </li>
          </ul>
          <!-- hamburger -->
          <div class="hamburger d-lg-none" onclick="openNav()" id="navbtn">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
       </div>
      </div>
      <!-- Mobile Menu -->
      <div id="myNav" class="overlay">
        <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
        <div class="overlay-content">
          <a href="/" class="menu_active menua">Home</a>
          <a href="about.php" target="blank_" >About</a>
          <li>
              <a href="<?php echo $siteurl;?>/fullstack-development">Fullstack Development <span></span>
              </a>
              <ul class='fullstack-development'>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development/node-and-Express-with-react.php">Node and Express with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\java-and-spring-boot-with-react.php">Java and Spring Boot with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\python-and-fast-api-with-react.php">Python and Fast API with React</a>
                </li>
                <li>
                  <a href="<?php echo $siteurl;?>/fullstack-development\php-and-laravel-with-react.php">PHP and Laravel with React</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#" data-id="blog" 
                >Service</a
              >
              <ul class='submenu'>
                <li><a href="#">Consulting</a>
                  <ul class='submenutwo'>
                  <li><a href="<?php echo $siteurl;?>/service/consulting/fullstack-web-development.php">Fullstack Web Development</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/consulting/design-system.php">Design System</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/consulting/front-end-development.php">Front-end Development</a></li>
                  </ul>
                </li>
                <li><a href="#">Training</a>
                  <ul class='submenutwo'>
                  <li><a href="<?php echo $siteurl;?>/service/training/fullstack-web-development-training-in-noida.php">Fullstack Web Development Training in Noida</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/react-js-training-in-noida.php">React JS Training in Noida</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/fullstack-web-development-training-in-lucknow.php">Fullstack Web Development Training in Lucknow</a></li>
                    <li><a href="<?php echo $siteurl;?>/service/training/react-js-training-in-lucknow.php">React JS Training in Lucknow</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          <!-- <a href="#" class="menua">Why JSChamps</a>
          <a href="#" class="menua">Upcoming Batch</a> -->
         <a href="https://www.jschamps.com/blog/" class="menua">Blog</a> 
          <a href="contact.php" >Contact Us</a>
        </div>
      </div>
    </nav>
    <!-- Menu End -->