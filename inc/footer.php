    <!-- Next Batch Start -->
    <section class="schedule py-4 menuToShow" id="upcoming-batch">
      <div class="container">
        <div class="gettouch" data-aos="zoom-in" data-aos-duration="1000">
          <div class="row">
            <div class="date">
              <h3>
                <span>Full</span>stack <span>Development</span> New
                <span>Batch</span>
              </h3>
              <!-- <h4 class="text-uppercase">
                29<sup class="text-lowercase">th</sup> August
              </h4> -->
              <p class="mt-5">Training: Hybrid Mode (Online + Offline)</p>
            </div>

            <hr class="d-lg-none" />

            <div class="row m-0 p-0">
              <div
                class="col-lg-4 order-lg-first text-center text-lg-start g-4"
              >
                <div class="address">
                  <h3>Noida Address</h3>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/location.svg"
                      alt="Address:-full-stack-web-developer"
                    />
                    G-118, Second Floor, Sector 63, Noida – 201301
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/telephone.svg"
                      alt="Phone:-full-stack-web-developer"
                    />
                    <a href="tel:01204265392">0120-426-5392</a>
                    <img
                      src="<?php echo $siteurl;?>/image/mobile.svg"
                      alt="Phone:-full-stack-web-developer"
                      style="width: 16px"
                    />
                    <a href="tel:09818008550">098180 08550</a>
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/whatsapp.svg"
                      alt="WhatsApp:-full-stack-web-developer"
                    />
                    <a href="//wa.me/+919818008550" target="_blank"
                      >098180 08550</a
                    >
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/mail.svg"
                      alt="Email:-full-stack-web-developer"
                    />
                    <a href="mailto:noida@jschamps.com">noida@jschamps.com</a>
                  </p>
                </div>
              </div>
              <!-- <hr class="d-lg-none"> -->
              <div
                class="col-lg-4 d-flex order-first justify-content-center g-4"
              >
                <div class="apply-now">
                  <a onclick="openForm()" style="cursor: pointer">Apply Now</a>
                </div>
              </div>
              <!-- <hr class="d-lg-none mt-4"> -->
              <div class="col-lg-4 text-center text-lg-end g-4">
                <div class="address">
                  <h3>Lucknow Address</h3>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/location.svg"
                      alt="Address:-full-stack-web-developer"
                    />
                    18, Chandganj Garden Aliganj, Near Chandra Shekhar Azad
                    Park, Lucknow - 226024
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/telephone.svg"
                      alt="Phone:-full-stack-web-developer"
                    />
                    <a href="tel:05224001372">0522-4001372</a>
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/whatsapp.svg"
                      alt="WhatsApp:-full-stack-web-developer"
                    />
                    <a href="//wa.me/+917617010381" target="_blank"
                      >07617010381</a
                    >
                  </p>
                  <p>
                    <img
                      src="<?php echo $siteurl;?>/image/mail.svg"
                      alt="Email:-full-stack-web-developer"
                    />
                    <a href="mailto:lucknow@jschamps.com"
                      >lucknow@jschamps.com</a
                    >
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Next Batch End -->
<!-- footer -->
    <section>
      <footer class="overflow-hidden d-none">
        <div class="row">
          <div class="col-md-10 mx-auto">
            <div class="row">
              <div class="col-6 col-md-4">
                <h5>Address</h5>
                <div class="main_address">
                  <div class="address">
                    <i class="bi bi-geo-alt"></i>
                  </div>
                  <div class="footer_text">
                    <p>Locations</p>
                  </div>
                </div>
              </div>
              <!-- <div class="col-6 col-md-4 d-none"  > 
            <h5>Useful Links</h5>
          <div class="link ">
           <div class="link_squire" ><i class="bi bi-link-45deg"></i></div>
           <div class="fooetr_text"><p>Locations</p></div>
          
          </div>
          </div> -->
              <div class="col-6 col-md-4">
                <h5>Social Media</h5>
                <div class="social">
                  <!-- <i class="bi bi-facebook"></i> -->
                  <i class="fa-brands fa-facebook-f"></i>
                  <i class="fa-brands fa-twitter"></i>
                  <!-- <i class="bi bi-linkedin"></i> -->
                  <i class="fa-brands fa-linkedin-in"></i>
                  <!-- <i class="bi bi-instagram"></i> -->
                  <i class="fa-brands fa-instagram"></i>
                </div>
              </div>
              <!-- <div class="col-6 col-md-3 " data-aos="fade-down-left" data-aos-duration="1000">
            <h5>NewsLetter</h5>
            <div class="news py-2">
              <input type="search" class="mb-3" >
              <button class=" bg-dark text-white">Subscribe</button>
            </div>
          </div> -->
            </div>
          </div>
        </div>
      </footer>
    </section>
   
    <!-- aos -->
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    <script>
      // NavBG
      var navbar = document.getElementById("nav");
      // console.log(navbar)
      window.onscroll = function () {
        if (
          document.body.scrollTop > 50 ||
          document.documentElement.scrollTop > 50
        ) {
          navbar.classList.add("bgcolor");
        } else if (
          document.body.scrollTop <= 50 ||
          document.documentElement.scrollTop <= 50
        ) {
          navbar.classList.remove("bgcolor");
        }
      };
    </script>

    <script src="<?php echo $siteurl;?>/js/index.js"></script>

    <!-- Bootstrap JavaScript Libraries -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script> -->

    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script> -->

    <script
      src="https://code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
      crossorigin="anonymous"
    ></script>
    <script>
      // $(window).scroll(function () {
      //   $('nav').toggleClass('bgcolor',$(this).scrollTop()>50)
      //   $('nav').toggleClass('bgTransparent',$(this).scrollTop()<50)
      // })
    </script>

    <script>
      var nav = document.getElementById("nav");

      function onLoadChangeColor() {
        if (
          document.body.scrollTop > 50 ||
          document.documentElement.scrollTop > 50
        ) {
          nav.classList.add("bgcolor");
        }
      }
    </script>
  </body>
</html>
