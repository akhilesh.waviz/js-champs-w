<?php include ("../inc/header.php");   ?>
<body onload="onLoadChangeColor()">
  <!-- Preloader Start -->
  <div id="preloader">
    <div class="sk-folding-cube">
      <div class="sk-cube1 sk-cube"></div>
      <div class="sk-cube2 sk-cube"></div>
      <div class="sk-cube4 sk-cube"></div>
      <div class="sk-cube3 sk-cube"></div>
    </div>
  </div>
  <!-- Preloader end -->

  <?php include ("../inc/menu.php");   ?>
  <!-- header Start Here -->
  <section class="banner overflow-hidden menuToShow">
    <div class="banner-content" id="about-menu"></div>
  </section>
  <!-- header end Here -->

  <!-- banner Start Here -->
  <div class="fullstack-banner">
    <div class="banner-image">
    <img src="./image/fullstack-banner.png" alt="fullstack-banner">
    </div>
  </div>
   <!-- banner End Here -->

    <!-- firsr section start -->
    <div class="container">
    <div class="row first-section">
      <div class="col-lg-6 col-md-12 col-sm-8">
      <div class="image">
                <img src="./image/node-react-express.webp" alt="node-react-express">
               
                </div>
        

      </div>
      <div class="col-lg-6 clo-md-12 col-sm-8">
      <div class="heading">
          <h3>Node and Express with React</h3>
          </div>
                   <div class="text"> 
                  <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. 
                  </p>
          </div>

      </div>

    </div>
    </div> 
    
        <!-- firsr section end -->



        <!-- second-section start -->
         <div class="container">
        <div class="row second-section">
          <div class="col-lg-6 col-md-10 col-sm-8">
          <div class="second-heading">
          <h3>Java and Spring Boot with React</h3>
          </div>
         <div class="second-para"> 
                  <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                  </p>
          </div>

          </div>

    <div class="col-lg-6 col-md-10 col-sm-8">
    <div class="second-image">
                <img src="./image/java-spring-boot-with-react.webp" alt="java-spring-boot-with-react">
                </div> 
    </div>

 
</div> 

    </div> 
    <!-- second section end -->

    <!-- thired section start -->

    <div class="container">
    <div class="row first-section">
      <div class="col-lg-6 col-md-12 col-sm-8">
      <div class="image">
                <img src="./image/python-and-fast-api-with-react.webp" alt="python-and-fast-api-with-react">
               
                </div>
        

      </div>
      <div class="col-lg-6 clo-md-12 col-sm-8">
      <div class="heading">
          <h3>Python and Fast API with React</h3>
          </div>
                   <div class="text"> 
                  <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. 
                  </p>
          </div>

      </div>

    </div>
    </div> 
    <!-- third section end -->

    <!-- fourth section start -->

    <div class="container">
        <div class="row second-section">
          <div class="col-lg-6 col-md-10 col-sm-8">
          <div class="second-heading">
            
          <h3>PHP and Laravel with React</h3>
          </div>
         <div class="second-para"> 
                  <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                   Consequuntur minima eius voluptate explicabo ullam, distinctio consequatur ut obcaecati
                    quia consectetur nesciunt provident dolore. Ab, illo minus. Blanditiis nobis voluptas nisi!
                  </p>
          </div>

          </div>

    <div class="col-lg-6 col-md-10 col-sm-8">
    <div class="second-image">
                <img src="./image/php-and-laravel-with-react.webp" alt="php-and-laravel-with-react ">
                </div> 
    </div>

 
</div> 

    </div> 
    <!-- fourth section end -->
    

    


   
            


  

  
    <?php include ("inc/footer.php");   ?>
  </div>
</body>



