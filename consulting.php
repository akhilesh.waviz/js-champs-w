<?php include ("inc/header.php");   ?>
<body onload="onLoadChangeColor()">
  <!-- Preloader Start -->
  <div id="preloader">
    <div class="sk-folding-cube">
      <div class="sk-cube1 sk-cube"></div>
      <div class="sk-cube2 sk-cube"></div>
      <div class="sk-cube4 sk-cube"></div>
      <div class="sk-cube3 sk-cube"></div>
    </div>
  </div>
  <!-- Preloader end -->
  <?php include ("inc/menu.php");   ?>
  <!-- header Start Here -->
  <section class="banner overflow-hidden menuToShow">
    <div class="banner-content" id="about-menu"></div>
  </section>

  <section class="devops container-fluid m-0 p-0">
    <div class="deveops_heading">
      <div class="container d-flex justify-content-center">
        <div class="left_clip"></div>
        <div class="container mid_clip">
          <h1 class="text-white text-center py-3">DevOps Consulting Services</h1>
        </div>
        <div class="right_clip"></div>
      </div>
    </div>
    <div class="container py-3">
      <div class="row">
        <div class="col-md-6">
          <div class="devops_text">
            <!-- <span>About us</span> -->
            <h2>Lorem ipsum dolor sit</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio! Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Iste quis eligendi distinctio
              dolores, aperiam minima, vitae veniam nihil voluptas porro
              possimus impedit dicta amet autem voluptatem magni id. Nisi,
              odio! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed perferendis vitae dolor distinctio
              consequatur quam sit blanditiis at? Voluptatibus, itaque.
            </p>
            <!-- <div class="devops_button">
              <button class="px-4 py-3">Read More</button>
            </div> -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="devops_image">
            <img src="./image/devops-img.png" class="img-fluid" />
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container consulting_service">
    <div class="devops_lorem_text">
      <!-- <spa>Lorem</spa> -->
      <h2>Lorem ipsum dolor sit</h2>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/hand.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/destop.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/progress.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/box.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/destop.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />

            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="hand">
          <img src="./image/laptop.png" class="img-fluid" />
          <div class="text_hand py-3">
            <h6>Lorem</h6>
            <hr />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit Iste
              quis eligendi distinctio dolores, aperiam minima, vitae veniam
              nihil voluptas porro possimus impedit dicta amet autem
              voluptatem magni id. Nisi, odio!
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
 

 
 <?php include('inc/footer.php'); ?>