  <?php include ("inc/header.php");   ?>
  <body onload="onLoadChangeColor()">
    <!-- Preloader Start -->
    <div id="preloader">
      <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
      </div>
    </div>
    <!-- Preloader end -->

    <?php include ("inc/menu.php");   ?>
    <!-- banner Start Here -->
    <section class="banner overflow-hidden menuToShow" >
      <div class="banner-content">
        <!-- <h3 class="text-uppercase">become a</h3> -->
        <!-- <h3 class="text-uppercase">fullstack developer</h3> -->
        <div
          class="fullstack py-2 py-lg-4"
          data-aos="fade-up-left"
          data-aos-duration="1000"
        >
          <h1 class="text-center text-uppercase">
            <!-- Become a fullstack developer -->
            <!-- Workforce champs who you need in your office! -->
            Grandeur of Full-Stack Web Development and Unveiling the Symphony of
            JavaScript
          </h1>
        </div>

        <div class="banner_text overflow-hidden">
          <p>
            <!-- Agile way to shape your software development career on Fullstack
            Software development track. Acquire knowledge and practical exposure
            about the latest software development trends, technologies and best
            practices. -->
            <!-- Looking for the right talent? Train Hire Deploy model of JS Champs
            becomes your savior likeit becomes for many leading companies. It
            addresses a huge problem of skill scarcity and fills positions to
            not let thriving companies compromise with their success. -->
            In the realm of web development, JSChamps stands as the beacon of
            innovation, the forge where aspiring developers evolve into seasoned
            full-stack virtuosos. It's not just a platform; it's an academy, a
            launchpad, and a sanctuary for those with a passion for crafting
            exceptional web experiences.
          </p>
          <div class="icon">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/icon.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661493251809"
              alt="icon-full-stack-developer"
              class="img-fluid"
            />
          </div>

          <div class="btn_banner py-2">
            <button
              class="btn btn px-4 py-2 mt-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
              onclick="openForm()"
              style="cursor: pointer"
            >
              Apply Now
            </button>
          </div>
          <div class="banner_pc">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/banner-pc.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988773216"
              alt="banner-pc-full-stack-developer"
              class="img-fluid"
            />
          </div>
        </div>
      </div>
    </section>

    <!-- banner Start End -->

    <!-- Work Section Start Here -->
    <!-- <section class="work overflow-hidden menuToShow" >
      <div class="hero">
        <h2 class="text-center" data-aos="zoom-in" data-aos-duration="1000">
          Not a <span>Training, </span><span>train-hire-deploy </span>Model
        </h2>
        <p class="w-50 text-center m-auto pb-3">
          Model is rapidly growing and substituting all conventional models,
          also known as talent-on-demand model. In this model training is
          completely different from orthodox software training.In our model
          candidates are going through scrupulous learn-develop-review mode
          learning with steady assessment.
        </p>
      </div>
      <div class="container">
        <div class="row">
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div>
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div>
          <div class="curv">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/curv.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498917727"
              alt="curv-full-stack-developer"
              class="img-fluid"
              data-aos="fade-up-left"
              data-aos-duration="1000"
            />
          </div> -->
          <!-- First Cercle -->
          <!-- <div
            class=" half-cirlce col-12 col-cercle col-md-6 col-lg-4 order-lg-first overflow-hidden p-4 "
            data-aos="fade-up-right"
            data-aos-duration="800"
          >
            <div class="cercle">
              <div class="yellow-cercle"></div>
              <div class="main-cercle">
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-1.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988683013"
                  alt="img-full-stack-developer"
                  class="img-fluid"
                />
                <h3>Train</h3>
                <p class="m-auto"> -->
                  <!-- Training and development exposure on industry live projects
                  Training that doesn’t drown to meet today’s demands of the
                  industries.
                </p>
              </div>
            </div>
          </div> -->
          <!-- Second Cercle -->
          <!-- <div
            class=" half-cirlce col-12 col-cercle col-lg-4 order-md-first overflow-hidden p-4"
            data-aos="flip-left"
            data-aos-duration="800"
          >
            <div class="brown-cercle">
              <div class="cercle">
                <div class="yellow-cercle"></div>
                <div class="main-cercle">
                  <img
                    src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-2.png?ik-sdk-version=javascript-1.4.3&updatedat=1661490015242"
                    alt="img-full-stack-developer"
                    class="img-fluid"
                  />
                  <h3>Hire</h3>
                  <p class="m-auto"> -->
                    <!-- Continuous assessment and evaluation during training to
                    hire. -->
                    <!-- Segments wearing veil of assessments to hire the best.
                  </p>
                </div>
              </div>
            </div>
          </div> -->
          <!-- Third Circle -->
          <!-- <div
            class="col-12 col-cercle col-md-6 col-lg-4 overflow-hidden p-4"
            data-aos="fade-up-left"
            data-aos-duration="800"
          >
            <div class="cercle">
              <div class="yellow-cercle"></div>
              <div class="main-cercle">
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/work-3.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661489383024"
                  alt="img-full-stack-developer"
                  class="img-fluid"
                />
                <h3>Deploy</h3>
                <p class="m-auto"> -->
                  <!-- Selected Champs will be deployed on customer or in-house
                  projects. -->
                  <!-- Tailored and basic training deploys the right candidate to the
                  right seat.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Work Section End Here-->

    <!-- Tools Section Start Here-->
    <section class="tools overflow-hidden py-5">
      <h2 class="text-uppercase d-none">frontend tools</h2>
      <div class="menu">
        <ul>
          <li>
            <p
              class="toolLink row_0 p-0 m-0 active"
              data-row="0"
              data-id="design"
            >
              Design & development
            </p>
          </li>
          <li>
            <p class="toolLink row_0 p-0 m-0" data-row="0" data-id="testing">
              Testing, build and Linting
            </p>
          </li>
          <li>
            <p class="toolLink row_0 p-0 m-0" data-row="0" data-id="build">
              NodeJS
            </p>
          </li>
        </ul>
      </div>
      <div class="tool row_0 d-block" id="design">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-6 col-md-3 g-3">
              <figure>
              <img src="./image/html&css&javascript.png" alt="html&css&javascript" data-aos="zoom-in"
                data-aos-duration="500">
                <!-- <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/html_css.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661499947582"
                  alt="Tool-full-stack-developer"
                  data-aos="zoom-in"
                  data-aos-duration="500"
                /> -->
                <figcaption>HTML5 & CSS3 & JavaScript</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3"> 
              <figure>
              <img src="./image/bootstrap&scss.png" alt="bootstrap&scss" data-aos="zoom-in"
                data-aos-duration="600">
                <!-- <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/sass.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661493055018"
                  alt="Tool-full-stack-developer"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                /> -->
                <figcaption>Bootstrap & SASS & LESS</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
              <img src="./image/figma-logo.png" alt="figma-logo" data-aos="zoom-in"
                data-aos-duration="700">
                <!-- <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/bootstrap_material_6LrKfh7Me.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661502078820"
                  alt="Tool-full-stack-developer"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                /> -->
                <figcaption>Figma</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
              <img src="./image/typescript.png" alt="typescript" data-aos="zoom-in"
                data-aos-duration="800">
                <!-- <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/javascript.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661499526770"
                  alt="Tool-full-stack-developer"
                  data-aos="zoom-in"
                  data-aos-duration="800"
                /> -->
                <figcaption>TypeScript</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/reactjs__oZRSRwc2E.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661501769857"
                  alt="Tool-full-stack-developer"
                  data-aos="zoom-in"
                  data-aos-duration="900"
                />
                <figcaption>React JS</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/vuejs.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494176935"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                />
                <figcaption>vue.JS</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/anguler_ehrOWYORkL.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988099731"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>angular</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/storybook_oYz7ergfwI.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988100857"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>Storybook</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
      <div class="tool row_0" id="testing">
        <div class="container">
          <div class="row justify-content-center">
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="500"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/jest.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661491491018"
                  alt="Tool-full-stack"
                />
                <figcaption>jest</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="600"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/enzymejs.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661495866245"
                  alt="Tool-full-stack"
                />
                <figcaption>Enzyme</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="700"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/karma.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497302299v"
                  alt="Tool-full-stack"
                />
                <figcaption>Karma</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="800"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mochachai.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496027753 "
                  alt="Tool-full-stack"
                />
                <figcaption>mochachai</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="900"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/eslint.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497913930"
                  alt="Tool-full-stack"
                />
                <figcaption>ESLint</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="1000"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/jslint.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661499191056"
                  alt="Tool-full-stack"
                />
                <figcaption>JSLint</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="1100"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/webpack.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661498816703 "
                  alt="Tool-full-stack"
                />
                <figcaption>webpack</figcaption>
              </figure>
            </div>
            <div
              class="col-6 col-md-3 g-3"
              data-aos="zoom-in"
              data-aos-duration="1100"
            >
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/npm_yarn.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497806974"
                  alt="Tool-full-stack"
                />
                <figcaption>npm & Yarn</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
      <div class="tool row_0" id="build">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/nodejs.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496733929"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="500"
                />
                <figcaption>NodeJS</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/express.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497424862"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                />
                <figcaption>Express JS</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/sql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497069610"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>SQL</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mysql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497216097"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>MYSql</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mongodp.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494487977"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="800"
                />
                <figcaption>MongoDB</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/aws_azure.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500544998"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="900"
                />
                <figcaption>Cloud (AWS & Azure)</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/microservices.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496565363"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                />
                <figcaption>Microservice</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/container.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500334308"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>Containers</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Tools Section End Here-->

    <!-- Tools Section Second Here-->
    <div class="tools overflow-hidden py-5">
      <!-- <h2>overflow-hidden</h2> -->
      <div class="menu">
        <ul>
          <li>
            <p class="toolLink row_1 p-0 m-0 active" data-row="1" data-id="one">
              JAVA Springboot
            </p>
          </li>
          <li>
            <p class="toolLink row_1 p-0 m-0" data-row="1" data-id="two">
              Python
            </p>
          </li>
          <li>
            <p class="toolLink row_1 p-0 m-0" data-row="1" data-id="three">
              PHP
            </p>
          </li>
        </ul>
      </div>
      <div class="tool row_1 d-block" id="one">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/core-java.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661499701864"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="500"
                />
                <figcaption>Core JAVA</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/spring-boot.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661495235525"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                />
                <figcaption>Spring Boot</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/sql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497069610"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>SQL</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mysql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497216097"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>MYSql</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mongodp.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494487977"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="800"
                />
                <figcaption>MongoDB</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/aws_azure.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500544998"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="900"
                />
                <figcaption>Cloud (AWS & Azure)</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/microservices.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496565363"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                />
                <figcaption>Microservice</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/container.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500334308"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>Containers</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
      <div class="tool row_1" id="two">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/python-3.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661491277871"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="500"
                />
                <figcaption>Python3</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/django.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500155526"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                />
                <figcaption>django</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/sql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497069610"
                  alt="Tool-full-stack"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>SQL</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mysql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497216097"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>MYSql</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mongodp.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494487977"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="800"
                />
                <figcaption>MongoDB</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/aws_azure.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500544998"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="900"
                />
                <figcaption>Cloud (AWS & Azure)</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/microservices.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496565363"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                />
                <figcaption>Microservice</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/container.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500334308"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>Containers</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
      <div class="tool row_1" id="three">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/php.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661495626454"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="500"
                />
                <figcaption>PHP</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/laravel.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661491671398"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="600"
                />
                <figcaption>laravel</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/sql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497069610"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>SQL</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mysql.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661497216097"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="700"
                />
                <figcaption>MYSql</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/mongodp.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494487977"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="800"
                />
                <figcaption>MongoDB</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/aws_azure.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661500544998"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="900"
                />
                <figcaption>Cloud (AWS & Azure)</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/wordpress.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661496247853"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="1000"
                />
                <figcaption>wordpress</figcaption>
              </figure>
            </div>
            <div class="col-6 col-md-3 g-3">
              <figure>
                <img
                  src="https://ik.imagekit.io/wavizkushinagar/jschamps/codiegnater.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661499282547"
                  alt="Tool-full-stack-web-developer"
                  data-aos="zoom-in"
                  data-aos-duration="1100"
                />
                <figcaption>codeIgniter</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Tools Section End Here-->

    <!-- product item -->
    <!-- <section class="product_item m-0 py-5 menuToShow" id="whyjschamps">
      <h2 class="text-uppercase d-none">our products</h2>
      <p class="text-center">
        Why <span>JSChamps</span> team is the <span> best choice</span> for
        <span>Fullstack Development</span>
      </p>
      <div class="container-fluid p-0 m-0">
        <div class="row p-0 m-0">
          <div class="col-md-12 p-0 m-0">
            <div class="row p-0 m-0">
              <div class="col-md-12 p-0 m-0 overflow-hidden">
                <div class="row p-0 m-0">
                  <div
                    class="col-md-6 first_col col-12 p-0 m-0"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_img m-0 p-0">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988732137"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                  <div
                    class="col-md-6 first_col_1 p-0"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_text p-0">
                      <p> -->
                        <!-- Learn from industry experts -->
                        <!-- JS Champs experts nurture the talent with the necessary
                        skills so that the candidates can become an asset and
                        not a liability to your business model.
                      </p>
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-12 main_col_third p-0 overflow-hidden">
                <div class="row product_row p-0 m-0">
                  <div
                    class="col-md-6 p-0 third_col"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_text_1">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                      <p> -->
                        <!-- Apply your learning and develop small software component -->
                        <!-- Our team of knowledgeable professionals is highly
                        trained in automation and other tools to educate for the
                        competition.
                      </p>
                    </div>
                  </div>
                  <div
                    class="col-md-6 col-12 p-0 third_col_1"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_img">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img_2.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988620517"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-12 p-0 main_fourth_col overflow-hidden">
                <div class="row p-0 m-0">
                  <div
                    class="col-md-6 first_col col-12 p-0"
                    data-aos="fade-left"
                    data-aos-duration="800"
                  >
                    <div class="product_img">
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product_img_3.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661489274528"
                        alt="img-full-stack-web-developer"
                        class="img-fuild"
                      />
                    </div>
                  </div>
                  <div
                    class="col-md-6 p-0 first_col_1"
                    data-aos="fade-right"
                    data-aos-duration="800"
                  >
                    <div class="product_text">
                      <p> -->
                        <!-- Get reviewed by experts and improvise your software
                        component -->
                        <!-- During the deployment resourcing, our team doubled check
                        the capability of every aptitude for 100% talent
                        retainability.
                      </p>
                      <img
                        src="https://ik.imagekit.io/wavizkushinagar/jschamps/product-mini.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661494996202"
                        alt="img-full-stack-web-developer"
                        class="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

        <!-- Next Batch End -->
    <?php include ("inc/footer.php");   ?>
    

   