<?php include ("../../inc/header.php");   ?>
<body onload="onLoadChangeColor()">
  <!-- Preloader Start -->
  <div id="preloader">
    <div class="sk-folding-cube">
      <div class="sk-cube1 sk-cube"></div>
      <div class="sk-cube2 sk-cube"></div>
      <div class="sk-cube4 sk-cube"></div>
      <div class="sk-cube3 sk-cube"></div>
    </div>
  </div>
  <!-- Preloader end -->
  
  <?php include ("../../inc/menu.php");   ?>
  <!-- start product development -->
  <section class="container-fluid m-0 p-0">
        <div class="production_mobile">
            <div class="container d-flex justify-content-center">
                <div class="left_clip"></div>
                <div class="container mid_clip">
                    <h1 class="text-white text-center py-3">REACT-JS-TRAINING-IN-LUCKNOW</h1>
                </div>
                <div class="right_clip"></div>
            </div>
        </div>
    </section>
    <!-- end product development -->

    <section class="container-fluid m-0 main_consulting">
        <div class="consulting_square">
            <img src="<?php echo $siteurl;?>/image/react-traning-in-lko.png" alt="react-traning-in-lko"/>

        </div>
        <div class="consulting_square_text">
            <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit</h2>
        </div>
        <div class="consulting_square_text_1">
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste quis
                eligendi distinctio dolores, aperiam minima, vitae veniam nihil
                voluptas porro possimus impedit dicta amet autem voluptatem magni id.
                Nisi, odio! Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Iste quis eligendi distinctio dolores, aperiam minima, vitae veniam
                nihil voluptas porro possimus impedit dicta amet autem voluptatem
                magni id. Nisi, odio! Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Iste quis eligendi distinctio dolores, aperiam
                minima, vitae veniam nihil voluptas porro possimus impedit dicta amet
                autem voluptatem magni id. Nisi, odio! Lorem ipsum dolor sit amet
                consectetur adipisicing elit. Iste quis eligendi distinctio dolores,
                aperiam minima, vitae veniam nihil voluptas porro possimus impedit
                dicta amet autem voluptatem magni id. Nisi, odio!
            </p>
        </div>
    </section>

    <section class="container py-3">
        <div class="row m-0 p-0  d-sm-flex justify-content-center">
            <div class="col-11 col-md-6">
                <div class="ui_image">
                    <img src="<?php echo $siteurl;?>/image/ui.png" alt="ui.png" />
                </div>
            </div>
            <div class="col-11 col-md-6">
                <div class="ui_text">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste quis
                        eligendi distinctio dolores, aperiam minima, vitae veniam nihil
                        voluptas porro possimus impedit dicta amet autem voluptatem magni
                        id. Nisi, odio! Lorem ipsum dolor sit amet consectetur adipisicing
                        elit. Iste quis eligendi distinctio dolores, aperiam minima, vitae
                        veniam nihil voluptas porro possimus impedit dicta amet autem
                        voluptatem magni id. Nisi, odio!
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus quisquam officia ab voluptatum
                        architecto,
                        commodi dolorum possimus ut soluta minus minima est et veniam libero magni eum voluptas aperiam
                        laboriosam
                        ratione, ullam assumenda voluptate non. Vero dolor quis ut fuga veniam adipisci amet odio sed
                        omnis sit,
                        minus odit ipsam.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum ex laudantium similique accusantium
                        commodi,
                        laboriosam a! Incidunt non quisquam at?
                    </p>
                </div>
            </div>
        </div>
    </section>

  <!-- end content -->
  <?php include ("../../inc/footer.php");   ?>