<?php include ("inc/header.php");   ?>
  <body onload="onLoadChangeColor()">
    <!-- Preloader Start -->
    <div id="preloader">
      <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
      </div>
    </div>
    <!-- Preloader end -->

    <?php include ("inc/menu.php");   ?>

<!-- banner Start Here -->
<section class="banner overflow-hidden menuToShow" >
      <div class="banner-content" id="about-menu">
        <!-- <h3 class="text-uppercase">become a</h3> -->
        <!-- <h3 class="text-uppercase">fullstack developer</h3> -->
        <!-- <div
          class="fullstack py-2 py-lg-4"
          data-aos="fade-up-left"
          data-aos-duration="1000"
        >
          <h1 class="text-center text-uppercase"> -->
            <!-- Become a fullstack developer -->
            <!-- This is Contact Us Page
          </h1>
        </div>

        <div class="banner_text overflow-hidden">
          <p> -->
            <!-- Agile way to shape your software development career on Fullstack
            Software development track. Acquire knowledge and practical exposure
            about the latest software development trends, technologies and best
            practices. -->
            <!-- This is Contact Us Page
          </p>
          <div class="icon">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/icon.png?ik-sdk-version=javascript-1.4.3&updatedAt=1661493251809"
              alt="icon-full-stack-developer"
              class="img-fluid"
            />
          </div>

          <div class="btn_banner py-2">
           
          </div>
          <div class="banner_pc">
            <img
              src="https://ik.imagekit.io/wavizkushinagar/jschamps/banner-pc.png?ik-sdk-version=javascript-1.4.3&updatedAt=1660988773216"
              alt="banner-pc-full-stack-developer"
              class="img-fluid"
            />
          </div>
        </div> -->
      </div>
    </section>

    <!-- banner Start End -->

    <section>
        <div id="google-map">
            <div style="height:260px;width:100%;max-width:100%;list-style:none; transition: none;overflow:hidden;">
                <div id="my-map-display" style="height:100%; width:100%;max-width:100%;">
                    <iframe style="height:100%;width:100%;border:0;" frameborder="0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d890.7103344069516!2d83.91436612918882!3d26.749438796534257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3993eb5cb631c861%3A0xb6398b35542882fb!2sWaviz%20Technologies%20(P)%20Ltd%20Kushinagar!5e0!3m2!1sen!2sin!4v1625915535466!5m2!1sen!2sin"></iframe>
                    <!-- <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Waviz+Technologies+(P)+Limited,+Sector+63,+Noida,+Uttar+Pradesh,+India&key=AIzaSyCPdxcfPtxOpDKUqeFxavaZ2wgVBP5G5mA"></iframe> -->
                </div>
                <style>
                    #my-map-display img {
                        max-width: none !important;
                        background: none !important;
                        font-size: inherit;
                    }
                </style>
            </div>
        </div>
    </section>
    <p> <br> <br> </p>
    <section class="content">
        <div class="container">
            <div class="col-row">
                <div class="one-half">
                    <div class="section-title align-left inner">
                        <h2 class="heading-big">Lets Do Something Great!</h2>
                    </div>
                    <form id="contact-form" method="post" action="php/submitemail.php">
                        <label for="name">Name: (*)</label><input class="textbox" type="text" name="name" id="name" />
                        <label for="email">E-mail: (*)</label><input class="textbox" type="text" name="email"
                            id="email" />
                        <label for="subject">Subject: </label><input class="textbox" type="text" name="subject"
                            id="subject" />
                        <label for="message">Message: (*)</label><textarea class="textbox" name="message"
                            id="message"></textarea>
                        <label for="check">Answer: (*)</label><span class="security-question"></span><input type="text"
                            name="check" id="check" /><br />
                        <button class="button small-btn" type="submit" name="submit" id="submit">Send Message</button>
                        <br />
                        <p id="message-outcome"></p>
                    </form>
                </div>
                <!-- end one-half -->
                <div class="one-half width-contact">
                    <div class="section-title align-left inner">
                        <h2 class="heading-big">Get in touch</h2>
                    </div>
                    <p id="contact-para">For more information on our services and custom solutions please contact us. We are just an
                        e-mail or phone call away. We look forward to hearing from you! </p>
                    <div class="contact-info col-row">
                        <div class="contact-info one-half main-contact">
                            <h3>Kushinagar - Office</h3>
                            <p>
                                <strong>Address:</strong> <br /> Main Road Kasia , Near Shiv Mandir <br>
                                Kushinagar-274402<br>
                            </p>
                            <p>
                                <strong>Tel:</strong> <a href="tel:07318167022">+91-731-181-7022</a> <br />
                                <strong>E-mail:</strong> <a href="mailto:gdubey@waviz.com">gdubey@waviz.com</a> <br />
                                <strong>Website:</strong> <a href="#">www.waviz.com</a> <br />
                            </p>
                        </div>

                        <!-- end  -->
                    </div>
                    <!-- end contact-info -->
                </div>
                <!-- end one-half -->
            </div>
            <!-- end col-row -->
        </div>
        <!-- end container-center -->
    </section>
    <!-- end content -->
    <?php include ("inc/footer.php");   ?>